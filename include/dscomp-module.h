/*
 * This file is part of dscomp.
 * 
 * Copyright 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#ifndef DSCOMP_MODULE_H
# define DSCOMP_MODULE_H

# define DSCOMP_OK 0
# define DSCOMP_EXITSTS 1
# define DSCOMP_NOT_FOUND 2
# define DSCOMP_ERROR 3

/* This is the construction function. It should allocate and
 * initialize a new data structure. The data structure is not allowed
 * to make use of static variables.
 * The function takes no arguments.
 * The function returns a pointer to the newly allocated and
 * initialized data structure or NULL on error. */
typedef void * dscomp_new_func_t (void);

/* This is the destruction function. It should deinitialize and free
 * the data structure.
 * Arguments:
 *  ds - a pointer to the data structure. Guratanteed to be non-NULL.
 * The function returns void. */
typedef void dscomp_destroy_func_t (void * const ds)
	__attribute__((nonnull));

/* This is the insertion function. It should insert an element with key
 * and value to the data structure ds.
 * Arguments:
 *  ds    - a pointer to the data structure. Guaranteed to be non-NULL.
 *  key   - the key of the element to insert.
 *  value - the value of the element to insert.
 * Returns DSCOMP_OK on success, DSCOMP_EXISTS if the element already
 * exists or DSCOMP_ERROR on any other error. */
typedef int dscomp_insert_func_t (void * const ds,
                                  const int key, 
                                  const int value)
	__attribute__((nonnull));

/* This is the search function. It should search for an element with key
 * as the key. Tha value of the element should be placed at the int
 * pointed to by valuep on success. If the element is not found or
 * another occured the content of *valuep may be left undefined.
 * Arguments:
 *  ds     - a pointer to the data structure. Guaranteed to be non-NULL.
 *  key    - the key of the element to search for.
 *  valuep - a pointer to the int where the value of the element
 *           should be stored. Guaranteed to be non-NULL.
 * Returns DSCOMP_OK on success, DSCOMP_NOT_FOUND if the element was
 * not found or DSCOMP_ERROR on any other error. */
typedef int dscomp_search_func_t (void * const ds,
                                  const int key,
                                  int * const valuep)
	__attribute__((nonnull));

/* This is the clear function. It should clear the data structure from
 * all elements, but leave the data structure initialized.
 * Arguments:
 *  ds - a pointer to the data structure. Guratanteed to be non-NULL.
 * Returns DSCOMP_OK on success or DSCOMP_ERROR failure. */
typedef int dscomp_clear_func_t (void * const ds)
	__attribute__((nonnull));

/* This is the sanity check function. It should sanity check the entire
 * data structure making sure it is OK.
 * Arguments:
 *  ds - a pointer to the data structure. Guratanteed to be non-NULL.
 * Returns DSCOMP_OK on success or DSCOMP_ERROR on failure. */
typedef int dscomp_check_func_t (void * const ds)
	__attribute__((nonnull));

struct dscomp_module_s {
	dscomp_new_func_t     * new_func;
	dscomp_destroy_func_t * destroy_func;
	dscomp_insert_func_t  * insert_func;
	dscomp_search_func_t  * search_func;
	dscomp_clear_func_t   * clear_func;
	dscomp_check_func_t   * check_func;
};
typedef struct dscomp_module_s dscomp_module_t;

#endif /* !DSCOMP_MODULE_H */
