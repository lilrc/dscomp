/*
 * This file is part of dscomp.
 * 
 * Copyright 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

/* pow() */
#if HAVE_MATH_H
# include <math.h>
#endif /* HAVE_MATH_H */

/* bool, false, true */
#if HAVE_STDBOOL_H
# include <stdbool.h>
#endif /* HAVE_STDBOOL_H */

/* EXIT_FAILURE, EXIT_SUCCESS, NULL */
#if HAVE_STDLIB_H
# include <stdlib.h>
#endif /* HAVE_STDLIB_H */

#include <gsl/gsl_errno.h>
#include <gsl/gsl_rng.h>

#include "common.h"
#include "elem-tree.h"

#define VERBOSE 0

#define N_ELEMENTS 10000

struct element_s {
	int key;
	int value;
};
typedef struct element_s element_t;

static bool __attribute__((nonnull))
elements_has_key ( const element_t * const elements,
                   const size_t elements_len,
                   const int key )
{
	for ( size_t i = 0; i < elements_len; ++i ) {
		if ( key == elements[i].key ) {
			return true;
		}
	}
	return false;
}

static int __attribute__((nonnull))
elem_sanity_check ( const elem_t * const elem,
                    const bool route_balanced,
                    size_t * const heightp,
                    size_t * const size )
{
#if VERBOSE
	printf("elem->key = %d\n", elem->key);
	if ( elem->left != NULL ) {
		printf("left->key = %d\n", elem->left->key);
	}
	if ( elem->right != NULL ) {
		printf("right->key = %d\n", elem->right->key);
	}
#endif /* VERBOSE */

	size_t left_height;
	size_t left_size;
	const elem_t * const left = elem->left;
	if ( left != NULL ) {
		if ( left->key >= elem->key ) {
			print_error("Sanity check failed\n");
			return 1;
		}

#if ELEM_TREE_PARENT_POINTER
		if ( left->parent != elem ) {
			print_error("Sanity check failed\n");
			fputs("Incorrect parent pointer\n", stderr);
			return 1;
		}
#endif /* ELEM_TREE_PARENT_POINTER */

		if ( elem_sanity_check(left, route_balanced, &left_height,
		                       &left_size) )
		{
			return 1;
		}

		if ( route_balanced ) {
			if ( left_size < pow(2, left_height - 1) ||
				 left_size >= pow(2, left_height) )
			{
				print_error("Sanity check failed\n");
				fputs("Left subtree is not route balanced\n", stderr);
				fprintf(stderr, "size = %lu, height = %lu\n", left_size,
				        left_height);
				return 1;
			}
		}
	} else /* if ( left == NULL ) */ {
		left_height = 0;
		left_size = 0;
	}

	if ( elem->n_left != left_size ) {
		print_error("Sanity check failed\n");
		fputs("Left subtree size mismatch\n", stderr);
		fprintf(stderr, "expected %lu, but got %lu\n", left_size,
		        elem->n_left);
		return 1;
	}

	size_t right_height;
	size_t right_size;
	const elem_t * const right = elem->right;
	if ( right != NULL ) {
		if ( right->key <= elem->key ) {
			print_error("Sanity check failed\n");
			return 1;
		}

#if ELEM_TREE_PARENT_POINTER
		if ( right->parent != elem ) {
			print_error("Sanity check failed\n");
			fputs("Incorrect parent pointer\n", stderr);
			return 1;
		}
#endif /* ELEM_TREE_PARENT_POINTER */

		if ( elem_sanity_check(right, route_balanced, &right_height,
		                       &right_size) )
		{
			return 1;
		}

		if ( route_balanced ) {
			if ( right_size < pow(2, right_height - 1) ||
				 right_size >= pow(2, right_height) )
			{
				print_error("Sanity check failed\n");
				fputs("Right subtree is not route balanced\n", stderr);
				fprintf(stderr, "size = %lu, height = %lu\n",
				        right_size, right_height);
				return 1;
			}
		}
	} else /* if ( right == NULL ) */ {
		right_height = 0;
		right_size = 0;
	}

	if ( elem->n_right != right_size ) {
		print_error("Sanity check failed\n");
		fputs("Right subtree size mismatch\n", stderr);
		fprintf(stderr, "expected %lu, but got %lu\n", right_size,
		        elem->n_right);
		return 1;
	}

	if ( right_height >= left_height ) {
		*heightp = right_height + 1;
	} else {
		*heightp = left_height + 1;
	}

	*size = left_size + right_size + 1;

	return 0;
}

static int __attribute__((nonnull))
elem_tree_sanity_check ( const elem_tree_t * const elem_tree,
                         const bool route_balanced )
{
	const elem_t * const root = elem_tree->root;
	if ( root == NULL ) {
		return 0;
	}

	size_t height;
	size_t size;
	if ( elem_sanity_check(root, route_balanced, &height, &size) ) {
		return 1;
	}

	if ( size != N_ELEMENTS ) {
		print_error("Sanity check failed: tree size mismatch\n");
		return 1;
	}

	if ( route_balanced ) {
		if ( size < pow(2, height - 1) || size >= pow(2, height) ) {
			print_error("Sanity check failed\n");
			fputs("Tree is not route balanced\n", stderr);
			fprintf(stderr, "size = %lu, height = %lu\n", size, height);
			return 1;
		}
	}

	return 0;
}

static int __attribute__((nonnull))
test ( gsl_rng * const rng,
       const bool ordered,
       const bool route_balanced )
{
	int retval = 0;

	elem_tree_t elem_tree;
	elem_tree_init(&elem_tree);

	element_t elements[N_ELEMENTS];
	for ( size_t i = 0; i < N_ELEMENTS; ++i ) {
		const int key = ordered ? i : gsl_rng_get(rng);
		if ( !ordered && elements_has_key(elements, i, key) ) {
			continue;
		}

		const int value = gsl_rng_get(rng);
		const elem_tree_error_t ret = elem_tree_insert(&elem_tree, key,
		                                               value);
		if ( ret ) {
			print_error("Could not insert key value pair into element "
			            "tree.\n");
			goto error;
		}

		elements[i].key = key;
		elements[i].value = value;
	}

	if ( route_balanced ) {
		elem_tree_balance(&elem_tree);
	}

	/* Try to insert elements with already inserted keys. It should
	 * fail. */
	for ( size_t i = 0; i < N_ELEMENTS; ++i ) {
		/* A value of 0 can be used here because the point is to make
		 * sure the key cannot be added. The value is irrelevant. */
		const int key = elements[i].key;
		const elem_tree_error_t ret = elem_tree_insert(&elem_tree, key,
		                                               0);
		if ( ret == ELEM_TREE_OK ) {
			print_error("Test failed: Insertion of an already added "
			            "key succeeded.\n");
			goto error;
		} else if ( ret != ELEM_TREE_EXISTS ) {
			print_error("Test failed: An error other than the already "
			            "existing occured during insertion.\n");
			goto error;
		}
	}

	/* This is done only for ordered data, because predicting the
	 * indices for the random data would be a costly operation and
	 * require some sorting which would be unnecessary work for a simple
	 * test. */
	if ( ordered ) {
		for ( size_t i = 0; i < N_ELEMENTS; ++i ) {
			int key, value;
			const int ret = elem_tree_get_by_index(&elem_tree, i, &key,
			                                       &value);
			if ( ret ) {
				print_error("Test failed: Could not get element by "
				            "index.\n");
				goto error;
			} else if ( key != elements[i].key ) {
				print_error("Test failed: Element gotten by index did "
				            "not match key.\n");
				goto error;
			} else if ( value != elements[i].value ) {
				print_error("Test failed: Element gotten by index did "
				            "not match value.\n");
				goto error;
			}
		}
	}

	if ( elem_tree_sanity_check(&elem_tree, route_balanced) ) {
		goto error;
	}

	if ( false ) {
	error:
		retval = 1;
	}

	elem_tree_deinit(&elem_tree);
	return retval;
}

int
main ( void )
{
	int exit_status = EXIT_SUCCESS;

	gsl_set_error_handler_off();
	gsl_rng * const rng = gsl_rng_alloc(gsl_rng_mt19937);
	if ( rng == NULL ) {
		print_error("Could not initialize random number generator: "
		            "%s\n", gsl_strerror(GSL_ENOMEM));
		return EXIT_FAILURE;
	}

	printf("Running tests on an unbalanced tree with random keys...\n");
	if ( test(rng, false, false) ) goto error;

	printf("Running tests on a balanced tree with random keys...\n");
	if ( test(rng, false, true) ) goto error;

	printf("Running tests on an unbalanced tree with ordered "
	       "keys...\n");
	if ( test(rng, true, false) ) goto error;

	printf("Running tests on a balanced tree with ordered keys...\n");
	if ( test(rng, true, true) ) goto error;

	if ( false ) {
	error:
		exit_status = EXIT_FAILURE;
	}

	gsl_rng_free(rng);

	return exit_status;
}
