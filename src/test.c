/*
 * This file is part of dscomp.
 * 
 * Copyright 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

/* assert() */
#include <assert.h>

/* INT_MAX, INT_MIN, UINT_MAX */
#if HAVE_LIMITS_H
# include <limits.h>
#endif /* HAVE_LIMITS_H */

#if HAVE_PTHREAD_H
# include <pthread.h>
#endif /* HAVE_PTHREAD_H */

/* bool, false, true */
#if HAVE_STDBOOL_H
# include <stdbool.h>
#endif /* HAVE_STDBOOL_H */

/* free(), malloc(), realloc()  */
#include <stdlib.h>

/* clock_gettime() */
#if HAVE_TIME_H
# include <time.h>
#endif /* HAVE_TIME_H */

#include <dscomp-module.h>

#include "common.h"
#include "elem-tree.h"
#include "test.h"

#define TEST_FAIL_INVRET "Invalid return value from search function"

struct meas_time_s {
	struct timespec ts1;
};
typedef struct meas_time_s meas_time_t;

static int __attribute__((nonnull))
meas_init ( meas_t * const meas )
{
	if ( pthread_mutex_init(&meas->mutex, NULL) ) {
		print_error_w_errno("pthread_mutex_init");
		return 1;
	}

	meas->n = 0;
	meas->avg = 0;

	return 0;
}

static void __attribute__((nonnull))
meas_deinit ( meas_t * const meas ) 
{
	if ( pthread_mutex_destroy(&meas->mutex) ) {
		print_error_w_errno("pthread_mutex_destroy");
	}
	return;
}

static void __attribute__((nonnull))
meas_time_start ( meas_time_t * const mt )
{
	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &mt->ts1);
	return;
}

static void __attribute__((nonnull))
meas_time_stop ( meas_t * const meas, const meas_time_t * const mt )
{
	struct timespec ts2;
	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &ts2);

	const double diff = (ts2.tv_sec - mt->ts1.tv_sec) * 1000000000 +
	                     ts2.tv_nsec - mt->ts1.tv_nsec;

	/* The formula is written to not overflow. */
	pthread_mutex_lock(&meas->mutex);
	meas->avg = meas->avg * ((double)meas->n / (meas->n + 1)) +
	             diff / (meas->n + 1);
	meas->n++;
	pthread_mutex_unlock(&meas->mutex);
	return;
}

test_t * __attribute__((nonnull))
test_new ( const module_t * const module,
           unsigned int elements,
           bool ordered )
{
	test_t * const test = malloc(sizeof(test_t));
	if ( test == NULL ) {
		print_error_w_errno("malloc");
		goto error1;
	}

	if ( pthread_mutex_init(&test->total_mutex, NULL) ) {
		print_error_w_errno("pthread_mutex_init");
		goto error2;
	}

	if ( meas_init(&test->insert) ) {
		goto error3;
	}

	if ( meas_init(&test->search_ordered_existing) ) {
		goto error4;
	}

	if ( meas_init(&test->search_ordered_nonexisting) ) {
		goto error5;
	}

	if ( meas_init(&test->search_random_existing) ) {
		goto error6;
	}

	if ( meas_init(&test->search_random_nonexisting) ) {
		goto error7;
	}

	if ( meas_init(&test->clear) ) {
		goto error8;
	}

	test->module = module;
	test->elements = elements;
	test->ordered = ordered;

	test->total = 0;
	test->next = NULL;

	return test;

error8:
	meas_deinit(&test->search_random_nonexisting);
error7:
	meas_deinit(&test->search_random_existing);
error6:
	meas_deinit(&test->search_ordered_nonexisting);
error5:
	meas_deinit(&test->search_ordered_existing);
error4:
	meas_deinit(&test->insert);
error3:
	pthread_mutex_destroy(&test->total_mutex);
error2:
	free(test);
error1:
	return NULL;
}

static void __attribute__((nonnull))
test_destroy ( test_t * const test )
{
	pthread_mutex_destroy(&test->total_mutex);
	free(test);
	return;
}

void
test_destroy_all ( test_t * test )
{
	while ( test != NULL ) {
		test_t * const next = test->next;
		test_destroy(test);
		test = next;
	}
	return;
}

static void __attribute__((nonnull))
test_fail ( const test_t * const test, const char * const reason )
{
	lock_stderr();

	fprintf(stderr,
	        "==== TEST FAILURE ====\n"
	        "Reason:   %s\n"
	        "Module:   %s\n"
	        "Elements: %u\n"
	        "Ordered:  %s\n"
	        "======================\n", reason, test->module->name,
	        test->elements, test->ordered ? "yes" : "no");

	print_error("Test failed\n");

	unlock_stderr();
	return;
}

static int __attribute__((nonnull))
insert_element ( test_t * const test,
                 void * const ds,
                 const int key,
                 const int value )
{
	const dscomp_module_t * const module = test->module->module;
	dscomp_insert_func_t * const insert_func = module->insert_func;

	meas_time_t mt;
	meas_time_start(&mt);
	const int ret = (*insert_func)(ds, key, value);
	meas_time_stop(&test->insert, &mt);
	if ( ret ) {
		test_fail(test, "Could not insert element into data structure");
		return 1;
	}

	return 0;
}

int __attribute__((nonnull))
test_ordered_insert ( test_t * const test,
                      order_info_t * const order_info,
                      gsl_rng * const rng,
                      void * const ds )
{
	order_info->key_start = gsl_rng_get(rng);
	order_info->value_start = gsl_rng_get(rng);

	/* The maximum value key_start can have without wrap-around. */
	const int max = INT_MAX - test->elements + 1;

	int key = order_info->key_start;
	int value = order_info->value_start;
	if ( key <= max && value <= max ) {
		const int key_max = key + test->elements;
		while ( key < key_max ) {
			if ( insert_element(test, ds, key, value) ) {
				return 1;
			}
			++key;
			++value;
		}
	} else /* if ( key > max || value > max ) */ {
		/* Although this loop can be optimized to not check key and
		 * value in each iteration, it is not worth it, as this loop is
		 * unlikely to be executed. */
		for ( unsigned int i = 0; i < test->elements; ++i ) {
			if ( insert_element(test, ds, key, value) ) {
				return 1;
			}
			if ( key != INT_MAX ) {
				++key;
			} else {
				key = INT_MIN;
			} 
			if ( value != INT_MAX ) {
				++value;
			} else {
				value = INT_MIN;
			}
		}
	}

	return 0;
}

int __attribute__((nonnull))
test_random_insert ( test_t * const test,
                     elem_tree_t * const elem_tree,
                     gsl_rng * const rng,
                     void * const ds )
{
	unsigned int n_left = test->elements;
	while ( n_left > 0 ) {
		const int key = gsl_rng_get(rng);
		const int value = gsl_rng_get(rng);
		const elem_tree_error_t ret = elem_tree_insert(elem_tree, key,
		                                               value);
		if ( ret == ELEM_TREE_OK ) {
			--n_left;
			if ( insert_element(test, ds, key, value) ) {
				return 1;
			}
			continue;
		} else if ( ret == ELEM_TREE_EXISTS ) {
			continue;
		} else {
			return 1;
		}
	}

	return 0;
}

static int __attribute__((nonnull))
insert_element_existing ( test_t * const test,
                          void * const ds,
                          const int key,
                          const int value )
{
	const dscomp_module_t * const module = test->module->module;
	dscomp_insert_func_t * const insert_func = module->insert_func;

	const int ret = (*insert_func)(ds, key, value);
	if ( ret == DSCOMP_OK ) {
		test_fail(test,
		          "Inserting an already existing element to the data "
		          "structure did not fail");
		return 1;
	} else if ( ret != DSCOMP_EXITSTS ) {
		test_fail(test, "Unknown error");
		return 1;
	}

	return 0;
}

int __attribute__((nonnull))
test_ordered_insert_existing ( test_t * const test,
                               const order_info_t * const order_info,
                               void * const ds )
{
	/* The maximum value key_start can have without wrap-around. */
	const int max = INT_MAX - test->elements + 1;

	int key = order_info->key_start;
	int value = order_info->value_start;
	if ( key <= max && value <= max ) {
		const int key_max = key + test->elements;
		while ( key < key_max ) {
			if ( insert_element_existing(test, ds, key, value) ) {
				return 1;
			}
			++key;
			++value;
		}
	} else /* if ( key > max || value > max ) */ {
		/* Although this loop can be optimized to not check key and
		 * value in each iteration, it is not worth it, as this loop is
		 * unlikely to be executed. */
		for ( unsigned int i = 0; i < test->elements; ++i ) {
			if ( insert_element_existing(test, ds, key, value) ) {
				return 1;
			}
			if ( key != INT_MAX ) {
				++key;
			} else {
				key = INT_MIN;
			} 
			if ( value != INT_MAX ) {
				++value;
			} else {
				value = INT_MIN;
			}
		}
	}

	return 0;
}

struct rie_s {
	test_t * test;
	void * ds;
};

static int __attribute__((nonnull))
rie_elem_func ( const int key,
                const int value,
                void * const argument )
{
	const struct rie_s * const rie = argument;
	return insert_element_existing(rie->test, rie->ds, key, value);
}

int __attribute__((nonnull))
test_random_insert_existing ( test_t * const test,
                              const elem_tree_t * const elem_tree,
                              void * const ds )
{
	struct rie_s rie;
	rie.test = test;
	rie.ds = ds;
	return elem_tree_recurse(elem_tree, &rie_elem_func, &rie);
}

static int __attribute__((nonnull))
expect_existing ( const test_t * const test,
                  const int ret,
                  const int value,
                  const int expected_value )
{

	if ( ret != DSCOMP_OK ) {
		if ( ret == DSCOMP_NOT_FOUND ) {
			test_fail(test, "Existing element was not found");
			return 1;
		} else if ( ret == DSCOMP_ERROR ) {
			test_fail(test, "Error");
			return 1;
		} else {
			test_fail(test, TEST_FAIL_INVRET);
			return 1;
		}
	}
	if ( value != expected_value ) {
		test_fail(test, "Got wrong value for element");
		return 1;
	}
	return 0;
}

static int __attribute__((nonnull))
expect_nonexisting ( const test_t * const test, const int ret )
{
	if ( ret == DSCOMP_OK ) {
		test_fail(test, "Nonexisting element was found");
		return 1;
	} else if ( ret == DSCOMP_ERROR ) {
		test_fail(test, "Error");
		return 1;
	} else if ( ret != DSCOMP_NOT_FOUND ) {
		test_fail(test, TEST_FAIL_INVRET);
		return 1;
	}
	return 0;
}

static int __attribute__((nonnull))
search_ordered_existing ( test_t * const test,
                          void * const ds,
                          const int key,
                          const int expected_value )
{
	const dscomp_module_t * const module = test->module->module;
	dscomp_search_func_t * const search_func = module->search_func;
	int value;
	meas_time_t mt;
	meas_time_start(&mt);
	const int ret = (*search_func)(ds, key, &value);
	meas_time_stop(&test->search_ordered_existing, &mt);
	if ( expect_existing(test, ret, value, expected_value) ) {
		return 1;
	}

	return 0;
}

static int __attribute__((nonnull))
search_ordered_nonexisting ( test_t * const test,
                             void * const ds,
                             const int key )
{
	const dscomp_module_t * const module = test->module->module;
	dscomp_search_func_t * const search_func = module->search_func;
	int value;
	meas_time_t mt;
	meas_time_start(&mt);
	const int ret = (*search_func)(ds, key, &value);
	meas_time_stop(&test->search_ordered_nonexisting, &mt);
	if ( expect_nonexisting(test, ret) ) {
		return 1;
	}

	return 0;
}

static int __attribute__((nonnull))
search_random_existing ( test_t * const test,
                         void * const ds,
                         const int key,
                         const int expected_value )
{
	const dscomp_module_t * const module = test->module->module;
	dscomp_search_func_t * const search_func = module->search_func;
	int value;
	meas_time_t mt;
	meas_time_start(&mt);
	const int ret = (*search_func)(ds, key, &value);
	meas_time_stop(&test->search_random_existing, &mt);
	if ( expect_existing(test, ret, value, expected_value) ) {
		return 1;
	}

	return 0;
}

static int __attribute__((nonnull))
search_random_nonexisting ( test_t * const test,
                            void * const ds,
                            const int key )
{
	const dscomp_module_t * const module = test->module->module;
	dscomp_search_func_t * const search_func = module->search_func;
	int value;
	meas_time_t mt;
	meas_time_start(&mt);
	const int ret = (*search_func)(ds, key, &value);
	meas_time_stop(&test->search_random_nonexisting, &mt);
	if ( expect_nonexisting(test, ret) ) {
		return 1;
	}

	return 0;
}

int __attribute__((nonnull))
test_ordered_search_ordered_existing ( test_t * const test,
                                       const order_info_t * const oi,
                                       void * const ds )
{
	/* The maximum value key_start can have without wrap-around. */
	const int max = INT_MAX - test->elements + 1;

	int key = oi->key_start;
	int value = oi->value_start;

	if ( key <= max && value <= max ) {
		const int key_max = key + test->elements;
		while ( key < key_max ) {
			if ( search_ordered_existing(test, ds, key, value) ) {
				return 1;
			}
			++key;
			++value;
		}
	} else /* if ( key > max || value > max ) */ {
		/* Although this loop can be optimized to not check key and
		 * value in each iteration, it is not worth it, as this loop is
		 * unlikely to be executed. */
		for ( unsigned int i = 0; i < test->elements; ++i ) {
			if ( search_ordered_existing(test, ds, key, value) ) {
				return 1;
			}
			if ( key != INT_MAX ) {
				++key;
			} else {
				key = INT_MIN;
			}
			if ( value != INT_MAX ) {
				++value;
			} else {
				key = INT_MIN;
			}
		}
	}

	return 0;
}

struct rsoe_s {
	test_t * test;
	void * ds;
};

static int __attribute__((nonnull))
rsoe_elem_func ( const int key,
                      const int value,
                      void * const argument )
{
	const struct rsoe_s * const rsoe = argument;
	return search_ordered_existing(rsoe->test, rsoe->ds, key, value);
}

int __attribute__((nonnull))
test_random_search_ordered_existing ( test_t * const test,
                                      const elem_tree_t * const et,
                                      void * const ds )
{
	struct rsoe_s rsoe;
	rsoe.test = test;
	rsoe.ds = ds;
	return elem_tree_recurse(et, &rsoe_elem_func, &rsoe);
}

int __attribute__((nonnull))
test_ordered_search_ordered_nonexisting ( test_t * const test,
                                          const order_info_t * const oi,
                                          gsl_rng * const rng,
                                          void * const ds )
{
	if ( test->elements == UINT_MAX ) {
		return 0;
	}

	/* The maximum value key_start can have without wrap-around. */
	const int max = INT_MAX - test->elements + 1;
	const int key_start = oi->key_start;
	int key = gsl_rng_get(rng);
	unsigned int n_left = test->elements;

	if ( key_start <= max ) {
		const int key_end = key_start + test->elements;
		if ( key >= key_start && key < key_end ) {
			key = key_end;
		}

		while ( n_left > 0 ) {
			assert(key < key_start || key >= key_end);
			if ( key < key_start ) {
				const unsigned int n_key_to_key_start = key_start - key;
				if ( n_key_to_key_start >= n_left ) {
					const int key_max = key + n_left;
					while ( key < key_max ) {
						if ( search_ordered_nonexisting(test, ds, key) )
						{
							return 1;
						}
						++key;
					}
					break;
				} else /* if ( n_key_to_key_start < n_left ) */ {
					n_left -= n_key_to_key_start;
					while ( key < key_start ) {
						if ( search_ordered_nonexisting(test, ds, key) )
						{
							return 1;
						}
						++key;
					}
					key = key_end;
					continue;
				}
			} else /* if ( key >= key_end ) */ {
				const unsigned int n_key_to_max = INT_MAX - key + 1;
				if ( n_key_to_max >= n_left ) {
					const int key_max = key + n_left;
					while ( key < key_max ) {
						if ( search_ordered_nonexisting(test, ds, key) )
						{
							return 1;
						}
						++key;
					}
					break;
				} else /* if ( n_key_to_max < n_left ) */ {
					n_left -= n_key_to_max;
					while ( key < INT_MAX ) {
						if ( search_ordered_nonexisting(test, ds, key) )
						{
							return 1;
						}
						++key;
					}
					if ( search_ordered_nonexisting(test, ds, INT_MAX) )
					{
						return 1;
					}
					if ( key_start != INT_MIN ) {
						key = INT_MIN;
					} else {
						key = key_end;
					}
					continue;
				}
			}
		}
	} else /* if ( key_start > max ) */ {
		const int key_end = INT_MIN + key_start - max - 1;
		if ( key >= key_start || key < key_end ) {
			key = key_end;
		}

		while ( n_left > 0 ) {
			assert(key >= key_end && key < key_start);
			const unsigned int n_key_to_key_start = key_start - key;
			if ( n_key_to_key_start >= n_left ) {
				const int key_max = key + n_left;
				while ( key < key_max ) {
					if ( search_ordered_nonexisting(test, ds, key) ) {
						return 1;
					}
					++key;
				}
				break;
			} else /* if ( n_key_to_key_start < n_left ) */ {
				n_left -= n_key_to_key_start;
				while ( key < key_start ) {
					if ( search_ordered_nonexisting(test, ds, key) ) {
						return 1;
					}
					++key;
				}
				key = key_end;
				continue;
			}
		}
	}

	return 0;
}

int __attribute__((nonnull))
test_random_search_ordered_nonexisting ( test_t * const test,
                                         const elem_tree_t * const et,
                                         gsl_rng * const rng,
                                         void * const ds )
{
	if ( test->elements == UINT_MAX ) {
		return 0;
	}

	int key = gsl_rng_get(rng);
	for ( unsigned int i = 0; i < test->elements; ++i ) {
		while ( elem_tree_has_key(et, key) ) {
			if ( key != INT_MAX ) {
				++key;
			} else {
				key = INT_MIN;
			}
		}

		if ( search_ordered_nonexisting(test, ds, key) ) {
			return 1;
		}
	}

	return 0;
}

int __attribute__((nonnull))
test_ordered_search_random_existing ( test_t * const test,
                                      const order_info_t * const oi,
                                      gsl_rng * const rng,
                                      void * const ds )
{
	const int key_start = oi->key_start;
	const int value_start = oi->value_start;
	const unsigned int key_start_to_max = INT_MAX - key_start;
	const unsigned int value_start_to_max = INT_MAX - value_start;
	for ( unsigned int i = 0; i < test->elements; ++i ) {
		const unsigned int j = gsl_rng_uniform_int(rng, test->elements);
		int key;
		if ( j <= key_start_to_max ) {
			key = key_start + j;
		} else /* if ( j > key_start_to_max ) */ {
			key = INT_MIN + j - key_start_to_max - 1;
		}

		int expected_value;
		if ( j <= value_start_to_max ) {
			expected_value = value_start + j;
		} else  /* if ( j > value_start_to_max ) */ {
			expected_value = INT_MIN + j - value_start_to_max - 1;
		}

		if ( search_random_existing(test, ds, key, expected_value) ) {
			return 1;
		} 
	}

	return 0;
}


int __attribute__((nonnull))
test_random_search_random_existing ( test_t * const test,
                                     const elem_tree_t * const et,
                                     gsl_rng * const rng,
                                     void * const ds )
{
	for ( unsigned int i = 0; i < test->elements; ++i ) {
		const unsigned int j = gsl_rng_uniform_int(rng, test->elements);
		int key, expected_value;
		if ( elem_tree_get_by_index(et, j, &key, &expected_value) ) {
			return 1;
		}
		if ( search_random_existing(test, ds, key, expected_value) ) {
			return 1;
		}
	}

	return 0;
}

int __attribute__((nonnull))
test_ordered_search_random_nonexisting ( test_t * const test,
                                         const order_info_t * const oi,
                                         gsl_rng * const rng,
                                         void * const ds )
{
	if ( test->elements == UINT_MAX ) {
		return 0;
	}

	/* The maximum value key_start can have without wrap-around. */
	const int max = INT_MAX - test->elements + 1;
	const int key_start = oi->key_start;

	if ( key_start <= max ) {
		const int key_end = key_start + test->elements;
		for ( unsigned int i = 0; i < test->elements; ++i ) {
			int key = gsl_rng_get(rng);
			while ( key >= key_start && key < key_end ) {
				key = gsl_rng_get(rng);
			}
			if ( search_random_nonexisting(test, ds, key) ) {
				return 1;
			}
		}
	} else /* if ( key_start > max ) */ {
		const int key_end = INT_MIN + key_start - max - 1;
		for ( unsigned int i = 0; i < test->elements; ++i ) {
			int key = gsl_rng_get(rng);
			while ( key >= key_start || key < key_end ) {
				key = gsl_rng_get(rng);
			}
			if ( search_random_nonexisting(test, ds, key) ) {
				return 1;
			}
		}
	}

	return 0;
}

int __attribute__((nonnull))
test_random_search_random_nonexisting ( test_t * const test,
                                        const elem_tree_t * const et,
                                        gsl_rng * const rng,
                                        void * const ds )
{
	if ( test->elements == UINT_MAX ) {
		return 0;
	}

	for ( unsigned int i = 0; i < test->elements; ++i ) {
		int key;
		do {
			key = gsl_rng_get(rng);
		} while ( elem_tree_has_key(et, key) );

		if ( search_random_nonexisting(test, ds, key) ) {
			return 1;
		}
	}
	return 0;
}

int __attribute__((nonnull))
test_check ( const test_t * const test, void * const ds )
{
	const dscomp_module_t * const module = test->module->module;
	dscomp_check_func_t * const check_func = module->check_func;
	if ( (*check_func)(ds) ) {
		test_fail(test, "Sanity check failed");
		return 1;
	}
	return 0;
}

int __attribute__((nonnull))
test_clear ( test_t * const test, void * const ds )
{
	const dscomp_module_t * const module = test->module->module;
	dscomp_clear_func_t * const clear_func = module->clear_func;
	meas_time_t mt;
	meas_time_start(&mt);
	const int ret = (*clear_func)(ds);
	meas_time_stop(&test->clear, &mt);
	if ( ret ) {
		test_fail(test, "Could not clean data structure");
		return 1;
	}
	return 0;
}
