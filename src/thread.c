/*
 * This file is part of dscomp.
 * 
 * Copyright 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#if HAVE_PTHREAD_H
# include <pthread.h>
#endif /* HAVE_PTHREAD_H */

/* clock() */
#if HAVE_TIME_H
# include <time.h>
#endif /* HAVE_TIME_H */

#include <gsl/gsl_errno.h>
#include <gsl/gsl_rng.h>
#include <dscomp-module.h>

#include "common.h"
#include "elem-tree.h"
#include "thread.h"

static int __attribute__((nonnull))
thread_test_ordered_search ( const dscomp_t * const dscomp,
                             test_t * const test,
                             const order_info_t * const order_info,
                             gsl_rng * const rng,
                             void * const ds)
{
	if ( dscomp->search_ordered ) {
		if ( dscomp->search_existing ) {
			if ( test_ordered_search_ordered_existing(test, order_info,
			                                          ds) )
			{
				return 1;
			}
		}
		if ( dscomp->search_nonexisting ) {
			if ( test_ordered_search_ordered_nonexisting(test,
			                                             order_info,
			                                             rng, ds) )
			{
				return 1;
			}
		}
	}

	if ( dscomp->search_random ) {
		if ( dscomp->search_existing ) {
			if ( test_ordered_search_random_existing(test, order_info,
			                                         rng, ds) )
			{
				return 1;
			}
		}
		if ( dscomp->search_nonexisting ) {
			if ( test_ordered_search_random_nonexisting(test,
			                                            order_info, rng,
			                                            ds) )
			{
				return 1;
			}
		}
	}

	return 0;
}

static int __attribute__((nonnull))
thread_test_random_search ( const dscomp_t * const dscomp,
                            test_t * const test,
                            const elem_tree_t * const elem_tree,
                            gsl_rng * const rng,
                            void * const ds )
{
	if ( dscomp->search_ordered ) {
		if ( dscomp->search_existing ) {
			if ( test_random_search_ordered_existing(test, elem_tree,
			                                         ds) )
			{
				return 1;
			}
		}
		if ( dscomp->search_nonexisting ) {
			if ( test_random_search_ordered_nonexisting(test,
			                                            elem_tree, rng,
			                                            ds) )
			{
				return 1;
			}
		}
	}

	if ( dscomp->search_random ) {
		if ( dscomp->search_existing ) {
			if ( test_random_search_random_existing(test, elem_tree,
			                                        rng, ds) )
			{
				return 1;
			}
		}
		if ( dscomp->search_nonexisting ) {
			if ( test_random_search_random_nonexisting(test, elem_tree,
			                                           rng, ds) )
			{
				return 1;
			}
		}
	}

	return 0;
}

static void * __attribute__((nonnull))
thread_routine ( void * const arg )
{
	dscomp_t * const dscomp = arg;
	test_t * test = dscomp->tests;
	const dscomp_module_t * module = test->module->module;
	void * ds = NULL;

	elem_tree_t elem_tree;
	elem_tree_init(&elem_tree);

	gsl_rng * const rng = gsl_rng_alloc(gsl_rng_mt19937);
	if ( rng == NULL ) {
		print_error("Could not initialize random number generator: "
		            "%s\n", gsl_strerror(GSL_ENOMEM));
		return NULL;
	}

	gsl_rng_set(rng, clock());

	while ( test != NULL ) {
		pthread_spin_lock(&dscomp->error_spin);
		const bool error = dscomp->error;
		pthread_spin_unlock(&dscomp->error_spin);
		if ( error ) {
			break;
		}

		pthread_mutex_lock(&test->total_mutex);
		if ( test->total < dscomp->repetitions ) {
			test->total++;
		} else {
			pthread_mutex_unlock(&test->total_mutex);
			test = test->next;
			continue;
		}
		pthread_mutex_unlock(&test->total_mutex);

		if ( ds == NULL ) {
		new_ds:
			ds = (*module->new_func)();
			if ( ds == NULL ) {
				print_error("Could not get new data structure");
				return NULL;
			}
		} else if ( module != test->module->module ) {
			(*module->destroy_func)(ds);
			module = test->module->module;
			goto new_ds;
		}

		if ( test->ordered ) {
			order_info_t order_info;
			if ( test_ordered_insert(test, &order_info, rng, ds) ) {
				goto error;
			}

			if ( dscomp->insert_existing ) {
				if ( test_ordered_insert_existing(test, &order_info,
				                                  ds) )
				{
					goto error;
				}
			}

			if ( dscomp->search_test ) {
				if ( thread_test_ordered_search(dscomp, test,
				                                &order_info, rng, ds) )
				{
					goto error;
				}
			}
		} else /* if ( !test->ordered ) */  {
			if ( test_random_insert(test, &elem_tree, rng, ds) ) {
				goto error;
			}

			if ( dscomp->insert_existing ) {
				if ( test_random_insert_existing(test, &elem_tree, ds) )
				{
					goto error;
				}
			}

			if ( dscomp->search_test ) {
				if ( dscomp->balance ) {
					elem_tree_balance(&elem_tree);
				}

				if ( thread_test_random_search(dscomp, test, &elem_tree,
				                               rng, ds) )
				{
					goto error;
				}
			}
		}

		if ( dscomp->check ) {
			if ( test_check(test, ds) ) {
				goto error;
			}
		}

		if ( test_clear(test, ds) ) {
			goto error;
		}

		if ( !test->ordered ) elem_tree_clear(&elem_tree);
	}

	if ( false ) {
	error:
		pthread_spin_lock(&dscomp->error_spin);
		dscomp->error = true;
		pthread_spin_unlock(&dscomp->error_spin);
	}

	if ( ds != NULL ) {
		(*module->destroy_func)(ds);
	}

	gsl_rng_free(rng);

	elem_tree_deinit(&elem_tree);

	return NULL;
}

int __attribute__((nonnull))
thread_run ( dscomp_t * const dscomp, pthread_t * const thread )
{
	if ( pthread_create(thread, NULL, &thread_routine, dscomp) ) {
		print_error_w_errno("pthread_create");
		return 1;
	}

	return 0;
}
