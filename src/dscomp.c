/*
 * This file is part of dscomp.
 * 
 * Copyright 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

/* assert() */
#include <assert.h>

#if HAVE_PTHREAD_H
# include <pthread.h>
#endif /* HAVE_PTHREAD_H */

/* fclose(), fopen(), fputs() */
#include <stdio.h>

/* memmove() */
#include <string.h>

#include "common.h"
#include "dscomp.h"
#include "module.h"
#include "thread.h"

#define DSCOMP_MIN_ELEMS_DEFAULT 512
#define DSCOMP_MAX_ELEMS_DEFAULT 4096
#define DSCOMP_REPETITIONS_DEFAULT 128
#define DSCOMP_ORDERED_DEFAULT 1
#define DSCOMP_RANDOM_DEFAULT 1
#define DSCOMP_STEP_TYPE_DEFAULT STEP_TYPE_ADD
#define DSCOMP_STEP_DEFAULT 512
#define DSCOMP_BALANCE_DEFAULT 0
#define DSCOMP_SEARCH_TEST_DEFAULT 1
#define DSCOMP_SEARCH_ORDERED_DEFAULT 1
#define DSCOMP_SEARCH_RANDOM_DEFAULT 1
#define DSCOMP_SEARCH_EXISTING_DEFAULT 1
#define DSCOMP_SEARCH_NONEXISTING_DEFAULT 1
#define DSCOMP_INSERT_EXISTING_DEFAULT 1
#define DSCOMP_CHECK_DEFAULT 1
#define DSCOMP_THREADS_DEFAULT 2

int __attribute__((nonnull))
dscomp_init ( dscomp_t * const dscomp )
{
	if ( module_init() ) {
		return 1;
	}

	if ( pthread_spin_init(&dscomp->error_spin,
	                       PTHREAD_PROCESS_PRIVATE) )
	{
		print_error_w_errno("pthread_spin_init");
		module_deinit();
		return 1;
	}

	dscomp->min_elems = DSCOMP_MIN_ELEMS_DEFAULT;
	dscomp->max_elems = DSCOMP_MAX_ELEMS_DEFAULT;
	dscomp->repetitions = DSCOMP_REPETITIONS_DEFAULT;
	dscomp->ordered = DSCOMP_ORDERED_DEFAULT;
	dscomp->random = DSCOMP_RANDOM_DEFAULT;

	dscomp->step_type = DSCOMP_STEP_TYPE_DEFAULT;
	dscomp->step = DSCOMP_STEP_DEFAULT;

	dscomp->balance = DSCOMP_BALANCE_DEFAULT;
	dscomp->search_test = DSCOMP_SEARCH_TEST_DEFAULT;
	dscomp->search_ordered = DSCOMP_SEARCH_ORDERED_DEFAULT;
	dscomp->search_random = DSCOMP_SEARCH_RANDOM_DEFAULT;
	dscomp->search_existing = DSCOMP_SEARCH_EXISTING_DEFAULT;
	dscomp->search_nonexisting = DSCOMP_SEARCH_NONEXISTING_DEFAULT;

	dscomp->insert_existing = DSCOMP_INSERT_EXISTING_DEFAULT;
	dscomp->check = DSCOMP_CHECK_DEFAULT;

	dscomp->threads = DSCOMP_THREADS_DEFAULT;
	dscomp->output_file_name = NULL;

	dscomp->error = false;

	module_list_init(&dscomp->modules);

	dscomp->tests = NULL;

	return 0;
}

void __attribute__((nonnull))
dscomp_deinit ( dscomp_t * const dscomp ) 
{
	pthread_spin_destroy(&dscomp->error_spin);
	free(dscomp->output_file_name);
	test_destroy_all(dscomp->tests);
	module_list_deinit(dscomp->modules);
	module_deinit();
	return;
}

int __attribute__((nonnull))
dscomp_module_load ( dscomp_t * const dscomp, const char * const name )
{
	module_t * const module = module_load(name);
	if ( module == NULL ) {
		return 1;
	}
	module_list_insert(&dscomp->modules, module);
	return 0;
}

int __attribute__((nonnull))
dscomp_prepare ( dscomp_t * const dscomp )
{
	for ( const module_t * module = dscomp->modules.first;
	      module != NULL;
	      module = module->next )
	{
		unsigned int elems = dscomp->min_elems;
		while ( elems <= dscomp->max_elems ) {
			if ( dscomp->ordered ) {
				test_t * const test = test_new(module, elems, true);
				if ( test == NULL ) {
					return 1;
				}
				test->next = dscomp->tests;
				dscomp->tests = test;
			}
			if ( dscomp->random ) {
				test_t * const test = test_new(module, elems, false);
				if ( test == NULL ) {
					return 1;
				}
				test->next = dscomp->tests;
				dscomp->tests = test;
			}

			if ( dscomp->step_type == STEP_TYPE_ADD ) {
				elems += dscomp->step;
			} else /* if ( dscomp->step_type == STEP_TYPE_MUL ) */ {
				elems *= dscomp->step;
			}
		}
	}
	return 0;
}

int __attribute__((nonnull))
dscomp_run ( dscomp_t * const dscomp )
{
	pthread_t * const threads = malloc(dscomp->threads *
	                                    sizeof(pthread_t));
	if ( threads == NULL ) {
		print_error_w_errno("malloc");
		return 1;
	}

	int ret = 0;

	unsigned int initialized = 0;
	while ( initialized < dscomp->threads ) {
		if ( thread_run(dscomp, threads + initialized) ) {
			ret = 1;
			goto end;
		}
		initialized++;
	}

end:
	for ( unsigned int i = 0; i < initialized; ++i ) {
		pthread_join(threads[i], NULL);
	}
	free(threads);

	if ( dscomp->error ) {
		ret = 1;
	}

	return ret;
}

#define INVALID_VALUE -1

struct column_s {
	const char * title;
	const char * module_name;
	bool ordered;
	double * rows;
	size_t n_rows;
};
typedef struct column_s column_t;

struct table_s {
	unsigned int * rows;
	size_t n_rows;

	column_t * columns;
	size_t n_columns;
};
typedef struct table_s table_t;

static int __attribute__((nonnull))
column_init ( column_t * const column,
              const size_t n_rows,
              const char * const title,
              const char * const module_name,
              const bool ordered )
{
	column->title = title;
	column->module_name = module_name;
	column->ordered = ordered;

	column->rows = malloc(n_rows * sizeof(double));
	if ( column->rows == NULL ) {
		print_error_w_errno("malloc");
		return 1;
	}

	column->n_rows = n_rows;
	for ( size_t i = 0; i < n_rows; ++i ) {
		column->rows[i] = INVALID_VALUE;
	}

	return 0;
}

static void __attribute__((nonnull))
column_deinit ( column_t * const column )
{
	free(column->rows);
	return;
}

static int __attribute__((nonnull))
column_insert_row ( column_t * const column,
                    const size_t row )
{
	const size_t move_rows = column->n_rows - row;
	column->n_rows++;
	column->rows = realloc(column->rows,
	                       column->n_rows * sizeof(double)); 
	if ( column->rows == NULL ) {
		print_error_w_errno("realloc");
		return 1;
	}

	memmove(column->rows + row + 1, column->rows + row,
	        move_rows * sizeof(double));
	column->rows[row] = INVALID_VALUE;

	return 0;
}

static void __attribute__((nonnull))
table_init ( table_t * const table )
{
	table->rows = NULL;
	table->n_rows = 0;
	table->columns = NULL;
	table->n_columns = 0;
	return;
}

static void __attribute__((nonnull))
table_deinit ( table_t * const table )
{
	for ( size_t i = 0; i < table->n_columns; ++i ) {
		column_deinit(table->columns + i);
	}
	free(table->rows);
	free(table->columns);
	return;
}

static int __attribute__((nonnull))
table_insert_column ( table_t * const table,
                      const char * const title,
                      const char * module_name,
                      const bool ordered,
                      const size_t column )
{
	const size_t move_columns = table->n_columns - column;

	table->n_columns++;
	table->columns = realloc(table->columns,
	                         table->n_columns * sizeof(column_t));
	if ( table->columns == NULL ) {
		print_error_w_errno("realloc");
		return 1;
	}

	memmove(table->columns + column + 1, table->columns + column,
	        move_columns * sizeof(column_t));
	if ( column_init(table->columns + column, table->n_rows, title,
	                 module_name, ordered) )
	{
		return 1;
	}

	return 0;
}

static int __attribute__((nonnull))
table_insert_row ( table_t * const table,
                   const unsigned int elements,
                   const size_t row )
{
	const size_t move_rows = table->n_rows - row;
	/* Make space for the new row. */
	table->n_rows++;
	table->rows = realloc(table->rows,
	                      table->n_rows * sizeof(unsigned int));
	if ( table->rows == NULL ) {
		print_error_w_errno("realloc");
		return 1;
	}

	/* Move all subsequent rows. */
	memmove(table->rows + row + 1, table->rows + row,
	        move_rows * sizeof(unsigned int));

	/* Fill in the row. */
	table->rows[row] = elements;

	for ( size_t i = 0; i < table->n_columns; ++i ) {
		if ( column_insert_row(table->columns + i, row) ) {
			return 1;
		}
	}

	return 0;
}

static int __attribute__((nonnull))
table_add ( table_t * const table,
            const unsigned int elements,
            const char * const title,
            const char * const module_name,
            const bool ordered,
            const double value )
{
	/* Make sure there is a row for this number of elements. */
	size_t row = 0;
	while ( row < table->n_rows && elements > table->rows[row] ) {
		++row;
	}
	if ( row == table->n_rows || elements != table->rows[row] ) {
		if ( table_insert_row(table, elements, row) ) {
			return 1;
		}
	}

	/* Find the right column */
	size_t column = 0;
	while ( true ) {
		if ( column == table->n_columns ) {
			goto insert_column;
		}

		const column_t * const this_column = table->columns + column;
		const int cmp1 = strcmp(this_column->title, title);
		if ( cmp1 < 0 ) {
			goto next;
		} else if ( cmp1 > 0 ) {
			goto insert_column;
		}

		/* cmp2 */
		if ( ordered && !this_column->ordered ) {
			goto insert_column;
		} else if ( !ordered && this_column->ordered ) {
			goto next;
		}

		const int cmp3 = strcmp(this_column->module_name, module_name);
		if ( cmp3 < 0 ) {
			goto next;
		} else if ( cmp3 > 0 ) {
			goto insert_column;
		} else /* if ( cmp3 == 0 ) */ {
			break;
		}

		if ( false ) {
		insert_column:
			if ( table_insert_column(table, title, module_name, ordered,
			                         column) )
			{
				return 1;
			}
			break;
		}

	next:
		++column;
	}

	table->columns[column].rows[row] = value;
	return 0;
}

static void __attribute__((nonnull))
table_print ( const table_t * const table, FILE * const file )
{
	fputs("Elements", file);
	for ( size_t i = 0; i < table->n_columns; ++i ) {
		const column_t * const column = table->columns + i;
		fprintf(file, ";%s - %s (%s)", column->title,
		        column->module_name,
		        column->ordered ? "ordered" : "random");
	}
	fputc('\n', file);

	for ( size_t row = 0; row < table->n_rows; ++row ) {
		fprintf(file, "%u", table->rows[row]);
		for ( size_t column = 0; column < table->n_columns; ++column ) {
			const double value = table->columns[column].rows[row];
			assert(value != INVALID_VALUE);
			fprintf(file, ";%.03f", value);
		}
		fputc('\n', file);
	}
	return;
}

int __attribute__((nonnull))
dscomp_print_results ( const dscomp_t * const dscomp )
{
	FILE * file;
	if ( dscomp->output_file_name != NULL ) {
		file = fopen(dscomp->output_file_name, "w");
		if ( file == NULL ) {
			print_error_w_errno("Could not open output file");
			return 1;
		}
	} else /* if ( dscomp->output_file_name == NULL ) */ {
		file = stdout;
	}

	table_t table;
	table_init(&table);

	for ( const test_t * test = dscomp->tests;
	      test != NULL;
	      test = test->next )
	{
		const char * const name = test->module->name;
		if ( table_add(&table, test->elements, "Insert", name,
		               test->ordered, test->insert.avg) )
		{
			return 1;
		}
		if ( table_add(&table, test->elements, "Clear", name,
		               test->ordered, test->clear.avg) )
		{
			return 1;
		}

		if ( !dscomp->search_test ) {
			continue;
		}
		if ( dscomp->search_ordered ) {
			if ( dscomp->search_existing ) {
				if ( table_add(&table, test->elements,
				               "Search existing ordered", name,
				               test->ordered,
				               test->search_ordered_existing.avg) )
				{
					return 1;
				}
			}
			if ( dscomp->search_nonexisting ) {
				if ( table_add(&table, test->elements,
				               "Search nonexisting ordered", name,
				               test->ordered,
				               test->search_ordered_nonexisting.avg) )
				{
					return 1;
				}
			}
		}
		if ( dscomp->search_random ) {
			if ( dscomp->search_existing ) {
				if ( table_add(&table, test->elements,
				               "Search existing random", name,
				               test->ordered,
				               test->search_random_existing.avg) )
				{
					return 1;
				}
			}
			if ( dscomp->search_nonexisting ) {
				if ( table_add(&table, test->elements,
				               "Search nonexisting random", name,
				               test->ordered,
				               test->search_random_nonexisting.avg) )
				{
					return 1;
				}
			}
		}
	}

	table_print(&table, file);
	table_deinit(&table);

	if ( file != stdout ) {
		if ( fclose(file) ) {
			print_error_w_errno("Could not close output file");
		}
	}

	return 0;
}
