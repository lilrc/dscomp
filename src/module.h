/*
 * This file is part of dscomp.
 * 
 * Copyright 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#ifndef MODULE_H
# define MODULE_H

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

# include <ltdl.h>

# include <dscomp-module.h>

struct module_s {
	lt_dlhandle handle;
	char * name;
	dscomp_module_t * module;

	struct module_s * next;
};
typedef struct module_s module_t;

struct module_list_s {
	module_t * first;
};
typedef struct module_list_s module_list_t;

/* Initializes the module layer.
 * Returns 0 on success or 1 on failure. */
int module_init ( void );

/* Deinitializes the module layer.
 * Returns 0 on success or 1 on failure. */
int module_deinit ( void );

void module_list_init ( module_list_t * const list )
	__attribute__((nonnull));
void module_list_deinit ( const module_list_t const list )
	__attribute__((nonnull));
void module_list_insert ( module_list_t * const list,
                         module_t * const module )
	__attribute__((nonnull));

module_t * module_load ( const char * const name )
	__attribute__((nonnull));
void module_unload ( module_t * const module )
	__attribute__((nonnull));

void module_print_list ( void );

#endif /* !MODULE_H */
