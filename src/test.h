/*
 * This file is part of dscomp.
 * 
 * Copyright 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#ifndef TEST_H
# define TEST_H

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

# if HAVE_PTHREAD_H
#  include <pthread.h>
# endif /* HAVE_PTHREAD_H */

/* bool */
# if HAVE_STDBOOL_H
#  include <stdbool.h>
# endif /* HAVE_STDBOOL_H */

# include <gsl/gsl_rng.h>

# include "elem-tree.h"
# include "module.h"

/* The information about the ordered data in a test. */
struct order_info_s {
	int key_start;
	int value_start;
};
typedef struct order_info_s order_info_t;

struct meas_s {
	pthread_mutex_t mutex;
	double avg;
	unsigned long int n;
};
typedef struct meas_s meas_t;

struct test_s {
	const module_t * module;
	unsigned int elements;
	bool ordered;

	pthread_mutex_t total_mutex;
	unsigned int total;

	meas_t insert;
	meas_t search_ordered_existing;
	meas_t search_ordered_nonexisting;
	meas_t search_random_existing;
	meas_t search_random_nonexisting;
	meas_t clear;

	struct test_s * next;
};
typedef struct test_s test_t;

test_t * test_new ( const module_t * const module,
                    unsigned int elements,
                    bool ordered )
	__attribute__((nonnull));

void test_destroy_all ( test_t * test );

int test_ordered_insert ( test_t * const test,
                          order_info_t * const order_info,
                          gsl_rng * const rng,
                          void * const ds )
	__attribute__((nonnull));

int  test_random_insert ( test_t * const test,
                          elem_tree_t * const elem_tree,
                          gsl_rng * const rng,
                          void * const ds )
	__attribute__((nonnull));

int
test_ordered_insert_existing ( test_t * const test,
                               const order_info_t * const order_info,
                               void * const ds )
	__attribute__((nonnull));

int test_random_insert_existing ( test_t * const test,
                                  const elem_tree_t * const elem_tree,
                                  void * const ds )
	__attribute__((nonnull));

int
test_ordered_search_ordered_existing ( test_t * const test,
                                       const order_info_t * const oi,
                                       void * const ds )
	__attribute__((nonnull));

int
test_random_search_ordered_existing ( test_t * const test,
                                      const elem_tree_t * const et,
                                      void * const ds )
	__attribute__((nonnull));

int
test_ordered_search_ordered_nonexisting ( test_t * const test,
                                          const order_info_t * const oi,
                                          gsl_rng * const rng,
                                          void * const ds )
	__attribute__((nonnull));

int
test_random_search_ordered_nonexisting ( test_t * const test,
                                        const elem_tree_t * const et,
                                        gsl_rng * const rng,
                                        void * const ds )
	__attribute__((nonnull));

int
test_ordered_search_random_existing ( test_t * const test,
                                      const order_info_t * const oi,
                                      gsl_rng * const rng,
                                      void * const ds )
	__attribute__((nonnull));

int
test_random_search_random_existing ( test_t * const test,
                                     const elem_tree_t * const et,
                                     gsl_rng * const rng,
                                     void * const ds )
	__attribute__((nonnull));

int
test_ordered_search_random_nonexisting ( test_t * const test,
                                         const order_info_t * const oi,
                                         gsl_rng * const rng,
                                         void * const ds )
	__attribute__((nonnull));

int 
test_random_search_random_nonexisting ( test_t * const test,
                                        const elem_tree_t * const et,
                                        gsl_rng * const rng,
                                        void * const ds )
	__attribute__((nonnull));

int test_check ( const test_t * const test, void * const ds )
	__attribute__((nonnull));

int test_clear ( test_t * const test, void * const ds )
	__attribute__((nonnull));

#endif /* !TEST_H */
