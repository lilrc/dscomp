/*
 * This file is part of dscomp.
 * 
 * Copyright 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#ifndef DSCOMP_H
# define DSCOMP_H

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

# if HAVE_PTHREAD_H
#  include <pthread.h>
# endif /* HAVE_PTHREAD_H */

/* FILE */
# include <stdio.h>

# include "module.h"
# include "test.h"

enum step_type_e {
	STEP_TYPE_ADD,
	STEP_TYPE_MUL
};
typedef enum step_type_e step_type_t;

struct dscomp_s {
	/* Test options. */
	unsigned int min_elems;
	unsigned int max_elems;
	unsigned int repetitions;
	int ordered;
	int random;

	step_type_t step_type;
	unsigned int step;

	/* Search options. */
	int balance;
	int search_test;
	int search_ordered;
	int search_random;
	int search_existing;
	int search_nonexisting;

	int insert_existing;
	int check;

	unsigned int threads;
	char * output_file_name;

	pthread_spinlock_t error_spin;
	bool error;

	module_list_t modules;

	test_t * tests;
};
typedef struct dscomp_s dscomp_t;

/* Initializes the dscomp program.
 * Returns 0 on success or 1 on failure. */
int dscomp_init ( dscomp_t * const dscomp ) __attribute__((nonnull));

/* Deinitializes the dscomp program. */
void dscomp_deinit ( dscomp_t * const dscomp ) __attribute__((nonnull));

int dscomp_module_load ( dscomp_t * const dscomp,
                         const char * const name )
	__attribute__((nonnull));

int dscomp_prepare ( dscomp_t * const dscomp ) __attribute__((nonnull));

int dscomp_run ( dscomp_t * const dscomp )
	__attribute__((nonnull));

int dscomp_print_results ( const dscomp_t * const dscomp )
	__attribute__((nonnull));

#endif /* !DSCOMP_H */
