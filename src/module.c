/*
 * This file is part of dscomp.
 * 
 * Copyright 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

/* va_end(), va_list, va_start() */
#include <stdarg.h>

/* fputc(), fputs(), vfprintf(), puts() */
#include <stdio.h>

/* malloc() */
#include <stdlib.h>

/* strdup(), strrchr() */
#include <string.h>

#include <ltdl.h>

#include "common.h"
#include "module.h"

/* A helper function to print an error from the ltdl library. */
static void __attribute__((format(printf, 1, 2)))
ltdl_error ( const char * const fmt, ... )
{
	print_error_prefix();

	if ( fmt != NULL ) {
		va_list ap;
		va_start(ap, fmt);
		vfprintf(stderr, fmt, ap);
		va_end(ap);

		fputs(": ", stderr);
	}

	const char * const error = lt_dlerror();
	if ( error != NULL ) {
		fputs(error, stderr);
		fputc('\n', stderr);
	} else {
		fputs("unknown error\n", stderr);
	}
	return;
}

int
module_init ( void )
{
	if ( lt_dlinit() ) {
		ltdl_error("Could not initialize the module layer");
		return 1;
	}

	/* Only allow modules from the module diectory. */
	if ( lt_dlsetsearchpath(DSCOMPDIR) ) {
		ltdl_error("Could not initialize the module layer");
		module_deinit();
		return 1;
	}

	return 0;
}

int
module_deinit ( void )
{
	if ( lt_dlexit() ) {
		ltdl_error("Could not deinitialize the module layer");
		return 1;
	}

	return 0;
}

void __attribute__((nonnull))
module_list_init ( module_list_t * const list )
{
	list->first = NULL;
	return;
}

void __attribute__((nonnull))
module_list_deinit ( const module_list_t const list )
{
	module_t * node = list.first;
	while ( node != NULL ) {
		module_t * const next = node->next;
		module_unload(node);
		node = next;
	}
	return;
}

void __attribute__((nonnull))
module_list_insert ( module_list_t * const list,
                     module_t * const module )
{
	module->next = list->first;
	list->first = module;
	return;
}

module_t * __attribute__((nonnull))
module_load ( const char * const name )
{
	module_t * const module = malloc(sizeof(module_t));
	if ( module == NULL ) {
		print_error_w_errno("Could not load module \"%s\"", name);
		return NULL;
	}

	module->name = strdup(name);
	if ( module->name == NULL ) {
		print_error_w_errno("Could not load module \"%s\"", name);
		free(module);
		return NULL;
	}

	module->handle = lt_dlopenext(name);
	if ( module->handle == NULL ) {
		ltdl_error("Could not load module \"%s\"", name);
		free(module->name);
		free(module);
		return NULL;
	}

	module->module = lt_dlsym(module->handle, "module");
	if ( module->module == NULL ) {
		ltdl_error("Could not load module \"%s\"", name);
		module_unload(module);
		return NULL;
	}

	module->next = NULL;

	return module;
}

void __attribute__((nonnull))
module_unload ( module_t * const module )
{
	if ( lt_dlclose(module->handle) ) {
		ltdl_error("Could not unload module \"%s\"", module->name);
	}

	free(module->name);
	free(module);

	return;
}

static int __attribute__((nonnull))
module_print_function ( const char * const filename, void * const data )
{
	const char * const name_begin = strrchr(filename, '/');
	if ( name_begin != NULL ) {
		puts(name_begin + 1);
	} else {
		puts(filename);
	}

	return 0;
}

void __attribute__((nonnull))
module_print_list ( void )
{
	lt_dlforeachfile(DSCOMPDIR, &module_print_function, NULL);
	return;
}
