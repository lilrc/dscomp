/*
 * This file is part of dscomp.
 * 
 * Copyright 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

/* assert() */
#include <assert.h>

/* bool, false, true */
#if HAVE_STDBOOL_H
# include <stdbool.h>
#endif /* HAVE_STDBOOL_H */

/* free(), malloc(), NULL */
#include <stdlib.h>

#include "common.h"
#include "elem-tree.h"

static inline void __attribute__((nonnull))
elem_init ( elem_t * const elem,
            const int key,
            const int value )
{
	elem->key = key;
	elem->value = value;
	elem->n_left = 0;
	elem->n_right = 0;
	elem->left = NULL;
	elem->right = NULL;

	/* elem->parent is not set here because it will be set upon
	 * insertion. */

	return;
}

static elem_t * __attribute__((nonnull))
elem_new ( elem_tree_t * const elem_tree,
           const int key,
           const int value )
{
	if ( elem_tree->recycled != NULL ) {
		elem_t * const elem = elem_tree->recycled;
		elem_tree->recycled = elem->right;
		elem_init(elem, key, value);
		return elem;
	}

	elem_t * const elem = malloc(sizeof(elem_t));
	if ( elem == NULL ) {
		print_error_w_errno("malloc");
		return NULL;
	}
	elem_init(elem, key, value);
	return elem;
}

static void __attribute__((nonnull))
elem_destroy ( elem_t * const elem )
{
	free(elem);
	return;
}

static void __attribute__((nonnull))
elem_recycle ( elem_tree_t * const elem_tree,
               elem_t * const elem )
{
	elem->right = elem_tree->recycled;
	elem_tree->recycled = elem;
	return;
}

static void __attribute__((nonnull))
elem_recycle_recursive ( elem_tree_t * const elem_tree,
                         elem_t * const elem )
{
	if ( elem->left != NULL ) {
		elem_recycle_recursive(elem_tree, elem->left);
	}
	if ( elem->right != NULL ) {
		elem_recycle_recursive(elem_tree, elem->right);
	}
	elem_recycle(elem_tree, elem);
	return;
}

void __attribute__((nonnull))
elem_tree_init ( elem_tree_t * const elem_tree )
{
	elem_tree->root = NULL;
	elem_tree->recycled = NULL;
	return;
}

void __attribute__((nonnull))
elem_tree_deinit ( elem_tree_t * const elem_tree )
{
	elem_tree_clear(elem_tree);

	elem_t * elem = elem_tree->recycled;
	while ( elem != NULL ) {
		elem_t * const next = elem->right;
		elem_destroy(elem);
		elem = next;
	}

	return;
}

void __attribute__((nonnull))
elem_tree_clear ( elem_tree_t * const elem_tree )
{
	if ( elem_tree->root != NULL ) {
		elem_recycle_recursive(elem_tree, elem_tree->root);
		elem_tree->root = NULL;
	}
	return;
}

#if ELEM_TREE_PARENT_POINTER
static void __attribute__((nonnull))
reset_counts ( elem_t * elem )
{
	elem_t * parent = elem->parent;
	while ( parent != NULL ) {
		assert(parent->left == elem || parent->right == elem);
		if ( parent->left == elem ) {
			--parent->n_left;
		} else /* if ( parent->right == elem ) */ {
			--parent->n_right;
		}
		elem = parent;
		parent = elem->parent;
	}
	return;
}

elem_tree_error_t __attribute__((nonnull))
elem_tree_insert ( elem_tree_t * const elem_tree,
                   const int key,
                   const int value )
{
	if ( elem_tree->root == NULL ) {
		elem_t * const root = elem_new(elem_tree, key, value);
		if ( root == NULL ) {
			return ELEM_TREE_NOMEM;
		}
		root->parent = NULL;
		elem_tree->root = root;
		return ELEM_TREE_OK;
	}

	/* Due to the nature of the input data (random keys in a wide range)
	 * the probability of a colliding element is small. Thus it is best
	 * to set the n_left and n_right members when searching downwards in
	 * the tree instead of stepping upwards on each successful inserion. 
	 * Upwards stepping is done when insertion failed. */

	elem_t * parent = elem_tree->root;
	while ( true ) {
		if ( key > parent->key ) {
			++parent->n_right;
			if ( parent->right != NULL ) {
				parent = parent->right;
				continue;
			} else /* if ( parent->right == NULL ) */ {
				elem_t * const elem = elem_new(elem_tree, key, value);
				if ( elem == NULL ) {
					--parent->n_right;
					reset_counts(parent);
					return ELEM_TREE_NOMEM;
				}
				elem->parent = parent;
				parent->right = elem;
				return ELEM_TREE_OK;
			}
		} else if ( key < parent->key ) {
			++parent->n_left;
			if ( parent->left != NULL ) {
				parent = parent->left;
				continue;
			} else /* if ( parent->left == NULL ) */ {
				elem_t * const elem = elem_new(elem_tree, key, value);
				if ( elem == NULL ) {
					--parent->n_left;
					reset_counts(parent);
					return ELEM_TREE_NOMEM;
				}
				elem->parent = parent;
				parent->left = elem;
				return ELEM_TREE_OK;
			}
		} else /* if ( key == parent->key ) */ {
			reset_counts(parent);
			return ELEM_TREE_EXISTS;
		}
	}
}
#else /* !ELEM_TREE_PARENT_POINTER */
static elem_tree_error_t __attribute__((nonnull))
elem_insert ( elem_tree_t * const elem_tree,
              elem_t * const parent,
              const int key,
              const int value )
{
	if ( key > parent->key ) {
		if ( parent->right != NULL ) {
			const elem_tree_error_t ret = elem_insert(elem_tree,
			                                          parent->right,
			                                          key, value);
			if ( ret == ELEM_TREE_OK ) {
				++parent->n_right;
			}
			return ret;
		} else /* if ( parent->right == NULL ) */ {
			parent->right = elem_new(elem_tree, key, value);
			if ( parent->right == NULL ) {
				return ELEM_TREE_NOMEM;
			}
			parent->n_right = 1;
			return ELEM_TREE_OK;
		}
	} else if ( key < parent->key ) {
		if ( parent->left != NULL ) {
			const elem_tree_error_t ret = elem_insert(elem_tree,
			                                          parent->left,
			                                          key, value);
			if ( ret == ELEM_TREE_OK ) {
				++parent->n_left;
			}
			return ret;
		} else /* if ( parent->left == NULL ) */ {
			parent->left = elem_new(elem_tree, key, value);
			if ( parent->left == NULL ) {
				return ELEM_TREE_NOMEM;
			}
			parent->n_left = 1;
			return ELEM_TREE_OK;
		}
	} else /* if ( key == parent->key ) */ {
		return ELEM_TREE_EXISTS;
	}
}

elem_tree_error_t __attribute__((nonnull))
elem_tree_insert ( elem_tree_t * const elem_tree,
                   const int key,
                   const int value )
{
	if ( elem_tree->root != NULL ) {
		return elem_insert(elem_tree, elem_tree->root, key, value);
	} else {
		elem_tree->root = elem_new(elem_tree, key, value);
		if ( elem_tree->root == NULL ) {
			return ELEM_TREE_NOMEM;
		}
		return ELEM_TREE_OK;
	}
}
#endif /* !ELEM_TREE_PARENT_POINTER */

int __attribute__((nonnull))
elem_tree_get_by_index ( const elem_tree_t * const elem_tree,
                         const size_t index,
                         int * const key,
                         int * const value )
{
	if ( elem_tree->root != NULL ) {
		elem_t * elem = elem_tree->root;
		if ( index > elem->n_left + elem->n_right ) {
			return 1;
		}

		size_t this_index = elem->n_left;
		while ( this_index != index ) {
			if ( this_index > index ) {
				elem = elem->left;
				this_index -= elem->n_right + 1;
			} else {
				elem = elem->right;
				this_index += elem->n_left + 1;
			}
		}
		*key = elem->key;
		*value = elem->value;
		return 0;
	} else {
		return 1;
	}
}

static int __attribute__((nonnull(1,2)))
elem_recurse ( const elem_t * const elem,
               elem_func_t * const func,
               void * const argument )
{
	if ( elem->left != NULL ) {
		if ( elem_recurse(elem->left, func, argument) ) {
			return 1;
		}
	}

	if ( (*func)(elem->key, elem->value, argument) ) {
		return 1;
	}

	if ( elem->right != NULL ) {
		if ( elem_recurse(elem->right, func, argument) ) {
			return 1;
		}
	}

	return 0;
}

int __attribute__((nonnull(1,2)))
elem_tree_recurse ( const elem_tree_t * const elem_tree,
                    elem_func_t * const func,
                    void * const argument )
{
	if ( elem_tree->root != NULL ) {
		return elem_recurse(elem_tree->root, func, argument);
	} else {
		return 0;
	}
}

bool __attribute__((nonnull))
elem_tree_has_key ( const elem_tree_t * const elem_tree, const int key )
{
	elem_t * elem = elem_tree->root;
	while ( elem != NULL ) {
		if ( key > elem->key ) {
			elem = elem->right;
		} else if ( key < elem->key ) {
			elem = elem->left;
		} else /* if ( key == elem->key ) */ {
			return true;
		}
	}
	return false;
}

static void __attribute__((nonnull))
compression ( elem_t * root, const size_t count )
{
	elem_t * scanner = root;
	for ( size_t i = 0; i < count; ++i ) {
		elem_t * const child = scanner->right;
		scanner->right = child->right;
#if ELEM_TREE_PARENT_POINTER
		/* It seems like child->right is always non-NULL in reality, so
		 * don't check for it. Just assert. */
		assert(child->right != NULL);
		child->right->parent = scanner;
#endif /* ELEM_TREE_PARENT_POINTER */

		scanner = scanner->right;
		child->right = scanner->left;
		child->n_right = scanner->n_left;
#if ELEM_TREE_PARENT_POINTER
		if ( scanner->left != NULL ) {
			scanner->left->parent = child;
		}
#endif /* ELEM_TREE_PARENT_POINTER */

		scanner->left = child;
		scanner->n_left = child->n_left + 1 + child->n_right;
#if ELEM_TREE_PARENT_POINTER
		child->parent = scanner;
#endif /* ELEM_TREE_PARENT_POINTER */
	}
	return;
}

void __attribute__((nonnull))
elem_tree_balance ( elem_tree_t * const elem_tree )
{
	if ( elem_tree->root == NULL ) return;

	elem_t pseudo_root;
	pseudo_root.left = NULL;
	pseudo_root.right = elem_tree->root;

	/* Convert the tree to a vine. */
	elem_t * vine_tail = &pseudo_root;
	elem_t * remainder = vine_tail->right;
	size_t size = 0;
	while ( remainder != NULL ) {
		if ( remainder->left == NULL ) {
			vine_tail = remainder;
			remainder = vine_tail->right;
			size++;
		} else {
			elem_t * const tmp_elem = remainder->left;
			remainder->left = tmp_elem->right;
			remainder->n_left = tmp_elem->n_right;
#if ELEM_TREE_PARENT_POINTER
			if ( tmp_elem->right != NULL ) {
				tmp_elem->right->parent = remainder;
			}
#endif /* ELEM_TREE_PARENT_POINTER */

			tmp_elem->right = remainder;
			tmp_elem->n_right = remainder->n_left + 1 +
			                     remainder->n_right;
#if ELEM_TREE_PARENT_POINTER
			remainder->parent = tmp_elem;
#endif /* ELEM_TREE_PARENT_POINTER */

			remainder = tmp_elem;
			vine_tail->right = remainder;
		}
	}

	/* Convert the vine to a tree. */
	size_t leaf_count = size + 1;
	size_t pow_of_two = 1;
	while ( 2 * pow_of_two <= leaf_count ) {
		pow_of_two *= 2;
	}
	leaf_count -= pow_of_two;
	compression(&pseudo_root, leaf_count);
	size -= leaf_count;
	while ( size > 1 ) {
		size = size / 2;
		compression(&pseudo_root, size);
	}

	elem_tree->root = pseudo_root.right;
#if ELEM_TREE_PARENT_POINTER
	elem_tree->root->parent = NULL;
#endif /* ELEM_TREE_PARENT_POINTER */

	return;
}
