/*
 * This file is part of dscomp.
 * 
 * Copyright 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#ifndef ELEM_TREE_H
# define ELEM_TREE_H

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

/* bool */
# if HAVE_STDBOOL_H
#  include <stdbool.h>
# endif /* HAVE_STDBOOL_H */

/* size_t */
# if HAVE_STDDEF_H
#  include <stddef.h>
# endif /* HAVE_STDDEF_H */

/* Define to 1 to implement the tree (and its corresponding functions)
 * using parent pointers, otherwise the tree and its operations will be
 * implemented using recursion.
 * 
 * The implementation using parent pointers is actually both slower and
 * more memory consuming so the implementation using recursion is
 * preferred.*/
#define ELEM_TREE_PARENT_POINTER 0

enum elem_tree_error_e {
	ELEM_TREE_OK,
	ELEM_TREE_NOMEM,
	ELEM_TREE_EXISTS
};
typedef enum elem_tree_error_e elem_tree_error_t;

struct elem_s {
	int key;
	int value;
	size_t n_left;
	size_t n_right;
	struct elem_s * left;
	struct elem_s * right;

#if ELEM_TREE_PARENT_POINTER
	struct elem_s * parent;
#endif /* ELEM_TREE_PARENT_POINTER */
};
typedef struct elem_s elem_t;

struct elem_tree_s {
	elem_t * root;
	elem_t * recycled;
};
typedef struct elem_tree_s elem_tree_t;

typedef int elem_func_t (const int key,
                         const int value,
                         void * const argument);

void elem_tree_init ( elem_tree_t * const elem_tree )
	__attribute__((nonnull));
void elem_tree_deinit ( elem_tree_t * const elem_tree )
	__attribute__((nonnull));
void elem_tree_clear ( elem_tree_t * const elem_tree )
	__attribute__((nonnull));

elem_tree_error_t elem_tree_insert ( elem_tree_t * const elem_tree,
                                     const int key,
                                     const int value )
	__attribute__((nonnull));

int elem_tree_get_by_index ( const elem_tree_t * const elem_tree,
                             const size_t index,
                             int * const key,
                             int * const value )
	__attribute__((nonnull));

int elem_tree_recurse ( const elem_tree_t * const elem_tree,
                        elem_func_t * const func,
                        void * const argument )
	__attribute__((nonnull(1,2)));

bool elem_tree_has_key ( const elem_tree_t * const elem_tree,
                         const int key )
	__attribute__((nonnull));

/* This function will balance the element tree using the
 * Day-Stout-Warren algorithm. */
void elem_tree_balance ( elem_tree_t * const elem_tree )
	__attribute__((nonnull));

#endif /* !ELEM_TREE_H */
