/*
 * This file is part of dscomp.
 * 
 * Copyright 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

/* false */
#if HAVE_STDBOOL_H
# include <stdbool.h>
#endif /* HAVE_STDBOOL_H */

/* puts() */
#include <stdio.h>

/* EXIT_FAILURE, EXIT_SUCCESS, free() */
#include <stdlib.h>

/* strcmp() */
#include <string.h>

#include <gop.h>
#include <gsl/gsl_errno.h>

#include "common.h"
#include "dscomp.h"

static char * step_type_string = NULL;

static gop_return_t __attribute__((nonnull))
print_version_callback ( gop_t * const gop )
{
	const char * const license = "\
License GPLv3+: GNU GPL version 3 or later\n\
<http://gnu.org/licenses/gpl.html>\n\
This is free software: you are free to change and redistribute it.\n\
There is NO WARRANTY, to the extent permitted by law.";

	puts(PACKAGE_STRING);
	puts("Copyright (C) 2014 Karl Linden");
	puts(license);

	return GOP_DO_EXIT;
}

static gop_return_t __attribute__((nonnull))
print_cflags ( gop_t * const gop )
{
	puts(DSCOMP_CFLAGS);
	return GOP_DO_EXIT;
}

static gop_return_t __attribute__((nonnull))
print_module_dir ( gop_t * const gop )
{
	puts(DSCOMPDIR);
	return GOP_DO_EXIT;
}

static gop_return_t __attribute__((nonnull))
print_module_list ( gop_t * const gop )
{
	module_print_list();
	return GOP_DO_EXIT;
}

static gop_return_t __attribute__((nonnull))
atexit_callback ( gop_t * const gop, void * const argument )
{
	dscomp_t * const dscomp = argument;
	dscomp_deinit(dscomp);
	free(step_type_string);
	return GOP_DO_EXIT;
}

int __attribute__((nonnull))
main ( const int argc, const char * const * const argv )
{
	int exit_status = EXIT_SUCCESS;

	dscomp_t dscomp;
	if ( dscomp_init(&dscomp) ) {
		return EXIT_FAILURE;
	}

	gop_t * const gop = gop_new();
	if ( gop == NULL ) {
		print_error("Could not get GOP context\n");
		goto error;
	}

	const gop_option_t search_options[] = {
		{"balance", '\0', GOP_YESNO, &dscomp.balance, NULL,
			"Balance internal search tree before running search tests",
			NULL},
		{"search-test", '\0', GOP_YESNO, &dscomp.search_test, NULL,
			"Run search tests", NULL},
		{"search-ordered", '\0', GOP_YESNO, &dscomp.search_ordered,
			NULL, "Search elements ordered", NULL},
		{"search-random", '\0', GOP_YESNO, &dscomp.search_random,
			NULL, "Search elements randomly", NULL},
		{"search-existing", '\0', GOP_YESNO, &dscomp.search_existing,
			NULL, "Search for existing elements", NULL},
		{"search-nonexisting", '\0', GOP_YESNO,
			&dscomp.search_nonexisting, NULL,
			"Search for nonexisting elements", NULL},
		GOP_TABLEEND
	};

	const gop_option_t build_options[] = {
		{"cflags", '\0', GOP_NONE, NULL, &print_cflags,
			"Print the CFLAGS needed to build a module and exit", NULL},
		{"module-dir", '\0', GOP_NONE, NULL, &print_module_dir,
			"Print the module directory and exit", NULL},
		GOP_TABLEEND
	};

	const gop_option_t options[] = {
		{"min-elems", 'm', GOP_UNSIGNED_INT, &dscomp.min_elems, NULL,
			"Minimum number of elemts to test", "MIN_ELEMS"},
		{"max-elems", 'M', GOP_UNSIGNED_INT, &dscomp.max_elems, NULL,
			"The maximum number of elements to test",
			"MAX_ELEMS"},
		{"repetitions", 'r', GOP_UNSIGNED_INT, &dscomp.repetitions,
			NULL,
			"The number of repetitions to be made for each number of "
			"elements", "REPETITIONS"},
		{"step-type", '\0', GOP_STRING, &step_type_string, NULL,
			"The step type to use", "add|mul"},
		{"step", 's', GOP_UNSIGNED_INT, &dscomp.step, NULL,
			"The step between two number of elements", "STEP"},

		{"ordered", '\0', GOP_YESNO, &dscomp.ordered, NULL,
			"Run tests with ordered data", NULL},
		{"random", '\0', GOP_YESNO, &dscomp.random, NULL,
			"Run tests with random data", NULL},

		{"insert-existing", '\0', GOP_YESNO, &dscomp.insert_existing,
			NULL,
			"Try to insert already existing into the data structures "
			"and make sure it does not work", NULL},

		{"check", '\0', GOP_YESNO, &dscomp.check, NULL,
			"Run sanity checks on all generated data strutures", NULL},

		{"threads", 't', GOP_UNSIGNED_INT, &dscomp.threads, NULL,
			"The number of threads to run", "THREADS"},

		{"output", 'o', GOP_STRING, &dscomp.output_file_name, NULL,
			"The output file to be used", "OUTPUT_FILE_NAME"},

		{"list-modules", 'l', GOP_NONE, NULL, &print_module_list,
			"Print a list of available modules", NULL},

		{"version", 'v', GOP_NONE, NULL, print_version_callback,
			"Print version and exit", NULL},
		GOP_TABLEEND
	};

	gop_atexit(gop, atexit_callback, &dscomp);
	gop_set_usage_option_string(gop, "[OPTION...] MODULES");
	gop_add_option_table(gop, NULL, options);
	gop_add_option_table(gop, "Search options:", search_options);
	gop_add_option_table(gop, "Build options:", build_options);
	gop_add_option_table(gop, GOP_AUTOHELP_TABLE_NAME,
	                     gop_autohelp_table);
	gop_parse_options(gop, argc, argv);

	const char * const * const unpaired_arguments =
		gop_get_unpaired_arguments(gop);
	if ( unpaired_arguments == NULL || unpaired_arguments[0] == NULL ) {
		print_error("Not enough arguments\n");
		gop_destroy(gop);
		free(step_type_string);
		goto error;
	} else {
		for ( const char * const * namep = unpaired_arguments;
		      *namep != NULL;
		      ++namep )
		{
			if ( dscomp_module_load(&dscomp, *namep) ) {
				gop_destroy(gop);
				free(step_type_string);
				goto error;
			}
		}
	}

	gop_destroy(gop);

	if ( step_type_string != NULL ) {
		if ( strcmp(step_type_string, "add") == 0 ) {
			dscomp.step_type = STEP_TYPE_ADD;
		} else if ( strcmp(step_type_string, "mul") == 0 ) {
			dscomp.step_type = STEP_TYPE_MUL;
		} else {
			print_error("Invalid step type: \"%s\". One of \"add\" and "
			            "\"mul\" is required.\n", step_type_string);
			goto error;
		}

		/* This is not needed anymore. */
		free(step_type_string);
	}

	/* Turn of the default error handler of GSL. All return values must
	 * be checked. */
	gsl_set_error_handler_off();

	if ( dscomp_prepare(&dscomp) ) {
		goto error;
	}

	if ( dscomp_run(&dscomp) ) {
		goto error;
	}

	if ( dscomp_print_results(&dscomp) ) {
		goto error;
	}

	if ( false ) {
	error:
		exit_status = EXIT_FAILURE;
	}

	dscomp_deinit(&dscomp);
	return exit_status;
}
