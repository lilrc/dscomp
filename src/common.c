/*
 * This file is part of dscomp.
 * 
 * Copyright 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

/* errno */
#include <errno.h>

#if HAVE_PTHREAD_H
# include <pthread.h>
#endif /* HAVE_PTHREAD_H */

/* va_end(), va_list, va_start(), vfprintf() */
#include <stdarg.h>

/* fputc(), fputs(), vfprintf() */
#include <stdio.h>

/* strerror() */
#include <string.h>

#include "common.h"

/* Don't risk scrambling different error messages! */
static pthread_mutex_t stderr_mutex =
	PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;

void
lock_stderr ( void )
{
	pthread_mutex_lock(&stderr_mutex);
	return;
}

void
unlock_stderr ( void )
{
	pthread_mutex_unlock(&stderr_mutex);
	return;
}

void
print_error_prefix ( void )
{
	fputs(PACKAGE ": ", stderr);
	return;
}

void __attribute__((nonnull, format(printf, 1, 2)))
print_error ( const char * const fmt, ... )
{
	lock_stderr();

	print_error_prefix();

	va_list ap;
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	unlock_stderr();
	return;
}

void __attribute__((nonnull, format(printf, 1, 2)))
print_error_w_errno ( const char * const fmt, ... )
{
	lock_stderr();

	print_error_prefix();

	va_list ap;
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	fputs(": ", stderr);
	fputs(strerror(errno), stderr);
	fputc('\n', stderr);

	unlock_stderr();
	return;
}
