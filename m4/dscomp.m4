#  
#  This file is part of dscomp.
#  
#  Copyright (C) 2014 Karl Lindén <lilrc@users.sourceforge.net>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  

# DSCOMP_INIT
# -----------
# This macro should be called when dscomp module should be built.
AC_DEFUN([DSCOMP_INIT],
[
	AC_REQUIRE([LT_INIT])dnl

	# Check for the dscomp program and export a variable so that it can
	# be set by the user if he or she wants.
	AC_CHECK_PROGS([DSCOMP], [dscomp], [no])
	AC_ARG_VAR([DSCOMP], [The dscomp program])
	AS_VAR_IF(
		[DSCOMP],
		[no],
		[AC_MSG_ERROR([could not find the dscomp program])]
	)

	# Get necessary cflags and let them too be set by the user if
	# needed.
	DSCOMP_CFLAGS=`${DSCOMP} --cflags`
	AS_IF(
		[test $? != 0],
		[AC_MSG_ERROR([could not get the dscomp cflags])]
	)
	AC_ARG_VAR(
		[DSCOMP_CFLAGS],
		[The CFLAGS needed to compile a dscomp module]
	)

	# Get the module directory and allow the user to set it if
	# necessary.
	DSCOMPDIR=`${DSCOMP} --module-dir`
	AS_IF(
		[test $? != 0],
		[AC_MSG_ERROR([could not get the dscomp module directory])]
	)
	AC_ARG_VAR(
		[DSCOMPDIR],
		[The directory where dscomp modules should be installed]
	)

	# Set the dscompdir variable so that it can be used by automake.
	AS_VAR_SET([dscompdir], [${DSCOMPDIR}])
	AC_SUBST([dscompdir])
])
