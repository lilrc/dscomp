#  
#  This file is part of dscomp.
#  
#  Copyright (C) 2014 Karl Lindén <lilrc@users.sourceforge.net>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

AC_PREREQ([2.69])
AC_INIT(
	[dscomp],
	[1.0],
	[https://bitbucket.org/lilrc/dscomp/issues],
	[dscomp],
	[https://bitbucket.org/lilrc/dscomp])
AC_CONFIG_SRCDIR([src/main.c])
AC_CONFIG_HEADERS([config.h])
AC_CONFIG_MACRO_DIR([m4])

LT_PREREQ([2.4.2])
LT_INIT([dlopen])
LT_CONFIG_LTDL_DIR([libltdl])

LTDL_INIT([recursive])

AM_INIT_AUTOMAKE([subdir-objects])

# Checks for programs.
AC_PROG_CC
AC_PROG_CC_C99
AS_VAR_IF(
	[ac_cv_prog_cc_c99],
	[no],
	[AC_MSG_ERROR([A C99 compatible C compiler is necessary])])

AM_PROG_CC_C_O

m4_ifndef(
	[PKG_PROG_PKG_CONFIG],
	[m4_fatal([pkg-config is required])])

# Checks for libraries.
PKG_CHECK_MODULES(
	[gop],
	[gop-1],
	[],
	[AC_MSG_ERROR([GOP was not found.
Please download and install it from:
https://bitbucket.org/lilrc/gop])])

PKG_CHECK_MODULES(
	[gsl],
	[gsl],
	[],
	[AC_MSG_ERROR([The GNU Scientific Library (GSL) was not found.])])

# Checks for header files.
AC_HEADER_STDBOOL
AC_HEADER_ASSERT
AC_CHECK_HEADERS([limits.h math.h pthread.h stddef.h time.h])

# Checks for typedefs, structures, and compiler characteristics.
AC_C_INLINE
AC_TYPE_SIZE_T

# Checks for library functions.
AC_FUNC_MALLOC
AC_FUNC_REALLOC
AC_CHECK_FUNCS([clock_gettime memmove strerror strrchr strdup])
AC_SEARCH_LIBS([pow], [m])

AC_ARG_ENABLE(
	[werror],
	[AS_HELP_STRING(
		[--enable-werror],
		[Pass -Werror when appropriate (default is no)])],
	[enable_werror=$enableval],
	[enable_werror=no]
)
AM_CONDITIONAL([ENABLE_WERROR], [test x$enable_werror = xyes])

AC_CONFIG_FILES([
	Makefile
	include/Makefile
	libltdl/Makefile
	m4/Makefile
	src/Makefile
	test/Makefile
])
AC_OUTPUT
